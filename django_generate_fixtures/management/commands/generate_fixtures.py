from django.core.management.base import (
    BaseCommand,
    CommandError,
)

from django.db import models
from django.db.models import Model
from django.core import serializers
import importlib

seen = []


def _get_data(obj):
    """
    fetch all the data of the object.
    follow the sets etc.
    """
    if not isinstance(obj, models.Model):
        raise Exception('Not a model')

    ans = []
    uid = repr(obj)
    if uid in seen:
        return ans
    seen.append(uid)

    # adding the reverse set
    for d in [d for d in dir(obj) if not d.startswith('_')]:
        try:
            attribut = getattr(obj, d)
        except:
            pass

        fields = [field.name for field in obj.__class__._meta.fields]
        fields += [field.name for field in obj.__class__._meta.many_to_many]
        if d in fields and isinstance(attribut, models.Manager):
            set_objs = attribut.all()
            for set_obj in set_objs:
                ans += _get_data(set_obj)  # so recurse

        if isinstance(attribut, models.Model):
            ans += _get_data(attribut)

    ans.append(obj)

    return ans


def generate_data(obj):
    ans = []
    if not isinstance(obj, Model):
        raise Exception('No model to follow')
    else:
        ans += _get_data(obj)

    return ans


class Command(BaseCommand):
    args = '<app>.<Model> <id_object> [<max_depth>]'
    help = """
           Generate fixtures
           for a given object, generate yaml fixtures to redirect
           to a file in order to have full hierarchy of models
           from this parent object.

           syntax example:
               python manage.py generate_fixtures 'core.models.Client' 364 > ./core/fixtures/test_fixtures.yaml
           """

    def add_arguments(self, parser):
        #parser.add_argument('app.models.model', type=str, required=True, help='Parent model to generate fixtures for')
        #parser.add_argument('object id', type=int, required=True, help='Primary key of model to include related objects of')
        parser.add_argument('app.models.model', type=str, help='Parent model to generate fixtures for')
        parser.add_argument('object_id', type=int, help='Primary key of model to include related objects of')
        #parser.add_argument('max_depth', type=int, help='Maximum depth to go when generating nested model references')

    def handle(self, *args, **options):
        try:
            app_model = options['app.models.model']
            pk = options['object_id']  # TODO:  split into list on commas present, and loop
        except:
            raise CommandError(self.help)

        module_name = '.'.join(app_model.split('.')[0:-1])
        model_name = app_model.split('.')[-1]
        module = importlib.import_module(module_name)
        if module:
            model = getattr(module, model_name)
            try:
                parent_obj = model.objects.get(pk=pk)
                self.stderr.write(
                    "fetched the parent obj {}\n".format(parent_obj))
            except:
                raise CommandError(
                    "didnt find the object with the pk {}".format(pk))

            data = generate_data(parent_obj)

            self.stdout.write(
                serializers.serialize("yaml", data, indent=4)
            )
            self.stderr.write("done\n\n")
        else:
            raise CommandError("No module named {}".format(module_name))
